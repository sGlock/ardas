import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TemplateService } from './template.service';
import { HtmlOutletDirective } from './html-outlet';

@NgModule({
  declarations: [
    AppComponent,
    HtmlOutletDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [TemplateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
