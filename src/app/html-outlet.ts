import {
  NgModule,
  Component,
  Directive,
  Input,
  ComponentRef,
  Compiler,
  ViewContainerRef,
  OnChanges,
  OnDestroy
} from '@angular/core';

import { CommonModule } from '@angular/common';
import {TemplateService} from './template.service';
import * as $ from 'jquery';

@Directive({
  selector: 'html-outlet'
})

export class HtmlOutletDirective implements OnChanges, OnDestroy {
  @Input() html: any;
  cmpRef: ComponentRef<any>;

  constructor(private vcRef: ViewContainerRef, private compiler: Compiler) { }

  ngOnChanges() {

    let html = this.html.template;
    let idTemplate = this.html.id;

    if (!html) { return; }

    if (this.cmpRef) {
      this.cmpRef.destroy();
    }

    const form = `<div class="editItem" #edit data-id=${idTemplate}><h4>Edit item</h4>
                <div><input class="editInput" type="text"></div>
                <div>
                  <select class="editSelect">
                    <option selected value="14">14</option>
                    <option value="18">18</option>
                    <option value="20">20</option>
                    <option value="22">22</option>
                    <option value="24">24</option>
                    <option value="26">26</option>
                    <option value="28">28</option>
                    <option value="30">30</option>
                    <option value="32">32</option>
                  </select>
                </div>
                <div><button (click)="cancelItem(edit)">Cancel</button><button (click)="saveItem(edit)">Save</button></div></div>`;

    @Component({
      selector: 'app-dynamic-comp',
      template: `${form} ${html}`
    })
    class DynamicHtmlComponent {

      constructor(private templateService: TemplateService) {}

      editItem(ref) {
        $(ref).closest('.template').find('.editable').removeClass('grey');
        $(ref).addClass('grey');

        const editElement = $(ref).closest('.template').prev();
        editElement.show();

        const editInput = editElement.find('.editInput');
        editInput.attr('id', ref.getAttribute('data-id'));
        editInput.val($(ref).text());

        const editSelect = editElement.find('.editSelect');
        const font = parseInt($(ref).css('font-size'), 10);
        editSelect.val(font);
      }

      saveItem(edit) {
        const value: string = $(edit).find('.editInput').val().toString();
        const fontSize: number = +$(edit).find('.editSelect').val();
        const strNum: number = +$(edit).find('.editInput').attr('id');
        const templateId: number = +$(edit).attr('data-id');

        $(edit).next().find('.editable').eq(strNum).text(value);
        $(edit).next().find('.editable').eq(strNum).css('font-size', fontSize + 'px');
        $(edit).next().find('.editable').removeClass('grey');
        $(edit).hide();

        const editObj = {
          value: value,
          fontSize: fontSize,
          strNum: strNum,
          templateId: templateId
        };

        this.templateService.editedItem(editObj);
      }

      cancelItem(edit) {
       $(edit).hide();
       $(edit).next().find('.editable').removeClass('grey');
      }
    }

    @NgModule({
      imports: [CommonModule ],
      declarations: [DynamicHtmlComponent]
    })

    class DynamicHtmlModule {}

    this.compiler.compileModuleAndAllComponentsAsync(DynamicHtmlModule)
      .then(factory => {
        const moduleRef = factory.ngModuleFactory.create(this.vcRef.parentInjector);
        const compFactory = factory.componentFactories.find(x => x.componentType === DynamicHtmlComponent);
        this.cmpRef = this.vcRef.createComponent(compFactory, 0, moduleRef.injector);
      });
  }

  ngOnDestroy() {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
  }
}
