import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TemplateService} from './template.service';
import {TemplateModel} from './template.model';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {

  title = 'Ardas test';
  templates: TemplateModel[] = [];
  showTemplate = false;
  html: any;

  constructor(private templateService: TemplateService) {
    templateService.editedItem$.subscribe(
      editObj => {
        this.editTemplate(editObj);
      });
  }

  ngOnInit() {
    this.templates = this.templateService.getTemplates();
  }

  onShowTemplate(template) {
    this.showTemplate = true;
    let html = template.template;

    /* parse template, add refs & events */
    const substr = `class='editable'`;
    const substrLength = 16;
    const gapStep = 43;
    let gap = 0;
    let arrFoundPos = [];
    let pos = 0;
    let hack = 0;

    while (true) {
      let foundPos = html.indexOf(substr, pos);
      if (foundPos === -1) { break; }
      arrFoundPos.push(foundPos);
      pos = foundPos + 1;
    }

    for (let x = 0; x < arrFoundPos.length; x++) {
      if (x > 10) { hack = 3; }
      gap = gapStep * x + hack;
      const ref = `ref${x}`;
      const attr = ` (click)='editItem(${ref})' data-id='${x}' #${ref}`;
      html = html.substr(0, (arrFoundPos[x]) + substrLength + gap) + attr + (html.substr(arrFoundPos[x] + substrLength + gap));
    }
    /* end parse template */

    template.template = html;
    this.html = template;
  }

  onBackToList() {
    this.showTemplate = false;
  }

  editTemplate(data) {
    this.templates[data.templateId - 1].modified = new Date().getTime();
    const editTemplate = $(this.templates[data.templateId - 1].template);

    editTemplate.find('.editable').eq(data.strNum).css('font-size', `${data.fontSize}px`);
    editTemplate.find('.editable').eq(data.strNum).text(data.value);

    this.templates[data.templateId - 1].template = editTemplate.get(0).outerHTML;
    this.onShowTemplate(this.templates[data.templateId - 1]);
  }
}
