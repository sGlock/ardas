import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  data = [
    {
      id: 1,
      name: 'One',
      template: `
        <div class='template'>
          <div class='editable'>
          One
          </div>
          <div class='container'>
            <div class='editable'>
              Two
            </div>
            <div class='editable'>
              Three
            </div>
          </div>
        </div>`,
      modified: 1516008350380
    },
    {
      id: 2,
      name: 'Two',
      template: `
        <div class='template'>
          <div class='container'>
            <div class='editable'>
              One
            </div>
            <div class='editable'>
              Two
            </div>
            <div class='editable'>
              Three
            </div>
          </div>
          <div class='editable'>
            Four
          </div>
        </div>`,
      modified: 1516008489616
    },
    {
      id: 3,
      name: 'Three',
      template: `
        <div class='template'>
          <div class='editable'>
            one
          </div>
          <div class='editable'>
            two
          </div>
          <div class='editable'>
            three
          </div>
          <div class='editable'>
            four
          </div>
          <div class='editable'>
            five
          </div>
          <div class='editable'>
            six
          </div>
          <div class='editable'>
            seven
          </div>
          <div class='editable'>
            eight
          </div>
          <div class='editable'>
            nine
          </div>
          <div class='editable'>
            ten
          </div>
          <div class='editable'>
            eleven
          </div>
          <div class='editable'>
            twelve
          </div>
        </div>`,
      modified: 1516008564742
    }
  ];

  constructor() { }
  public getTemplates() {
    return this.data;
  }

  // Observable string sources
  private editedItemSource = new Subject<any>();

  // Observable string streams
  editedItem$ = this.editedItemSource.asObservable();

  // Service message commands
  editedItem(editObj) {
    console.log('PUT to server', editObj);
    this.editedItemSource.next(editObj);
  }
}
