export class TemplateModel {
  constructor(public id: number,
              public name: string,
              public template: string,
              public modified: number) {
  }
}
